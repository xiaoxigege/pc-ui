const { im, distribution } = require('./index')

module.exports = {
	NODE_ENV: '"production"',
	ENV_CONFIG: '"prod"',
  IM: im,
	DISTRIBUTION: distribution,
  API_CHECK: '"http://47.115.46.227:8081"'
}
