const { im, distribution } = require('./index')

module.exports = {
	NODE_ENV: '"development"',
	ENV_CONFIG: '"dev"',
  IM: im,
	DISTRIBUTION: distribution,
  API_CHECK: '"http://localhost:8081"'
}
