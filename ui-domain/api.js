/**
 * Created by Andste on 2018/7/2.
 * base    : 基础业务API
 * buyer   : 买家API
 * seller  : 商家中心API
 * admin   ：后台管理API
 */

module.exports = {
  // 开发环境
  dev: {
    base  : 'https://base-bbc-api.shoptnt.cn/base',
    buyer : 'https://shop-bbc-api.shoptnt.cn',
    seller: 'https://seller-bbc-api.shoptnt.cn',
    admin : 'https://manager-bbc-api.shoptnt.cn'
  },
  // 测试环境
  test: {
    base  : window.__ENV__.API_BASE   || 'https://base-bbc-api.shoptnt.cn',
    buyer : window.__ENV__.API_BUYER  || 'https://shop-bbc-api.shoptnt.cn',
    seller: window.__ENV__.API_SELLER || 'https://seller-bbc-api.shoptnt.cn',
    admin : window.__ENV__.API_ADMIN  || 'https://manager-bbc-api.shoptnt.cn'
  },
  // 生产环境
  pro: {
    base  : window.__ENV__.API_BASE || 'https://base-bbc-api.shoptnt.cn',
    buyer : window.__ENV__.API_BUYER || 'https://shop-bbc-api.shoptnt.cn',
    seller: window.__ENV__.API_SELLER || 'https://seller-bbc-api.shoptnt.cn',
    admin : window.__ENV__.API_ADMIN || 'https://manager-bbc-api.shoptnt.cn'
  }
}
